/* eslint-disable no-nested-ternary */
import PropTypes from 'prop-types';
import { useState } from 'react';
import Paper from '@mui/material/Paper';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import EditIcon from '@mui/icons-material/Edit';
// import IconCheck from '@static/images/icon-check.svg';
import CheckIcon from '@mui/icons-material/Check';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import Button from '@mui/material/Button';
import { selectTheme } from '@containers/Settheme/selector';
import { selectTodo, selectTodoActive, selectTodoFilter } from '@containers/Settodo/selector';
import { connect, useDispatch } from 'react-redux';
// import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
// import { setTodo } from '@containers/Settodo/actions';
import {
  setTodoRemove,
  setTodoActive,
  setTodoFilter,
  setTodoClear,
  setTodoIsediting,
  setTodoEdit,
} from '@containers/Settodo/actions';
import CloseIcon from '@mui/icons-material/Close';

import classes from './style.module.scss';

const List = ({ theme, todos, todofilter }) => {
  const [editingTask, setEditingTask] = useState('');
  const [alreadyEdit, setAlreadyEdit] = useState(false);
  // console.log(editingTask);
  const dispatch = useDispatch();

  const handleRemoveTodo = (id) => {
    const rev = todos.filter((todo) => todo.id !== id);

    dispatch(setTodoRemove(rev));
  };

  const handleClearCompleted = (clearcompleted) => {
    const celar = todos.filter((todo) => todo.status !== clearcompleted);

    dispatch(setTodoClear(celar));
  };

  const handleActive = (id) => {
    const toggleActive = todos.map((todo) =>
      todo.id === id
        ? {
            ...todo,
            status: todo.status === 'active' ? 'completed' : 'active',
          }
        : todo
    );

    dispatch(setTodoActive(toggleActive));
  };

  const closeEdit = (id, task) => {
    setAlreadyEdit(true);
    const toggleEdit = todos.map((todo) => (todo.id === id ? { ...todo, isEditing: !todo.isEditing } : todo));
    dispatch(setTodoIsediting(toggleEdit));
    setEditingTask(task);
    setAlreadyEdit(false);

    // dispatch(setTodoEdit())
  };

  const editTodo = (id, task) => {
    if (alreadyEdit === false) {
      setAlreadyEdit(true);
      const toggleEdit = todos.map((todo) => (todo.id === id ? { ...todo, isEditing: !todo.isEditing } : todo));
      dispatch(setTodoIsediting(toggleEdit));
      setEditingTask(task);
    }

    // dispatch(setTodoEdit())
  };

  const handleTermTask = (event) => {
    setEditingTask(event.target.value);
  };

  const handleSaveTask = (event, id) => {
    if (event.key === 'Enter' && event.target.value.trim() !== '') {
      event.preventDefault();

      const saveTask = todos.map((todo) =>
        todo.id === id ? { ...todo, task: editingTask, isEditing: !todo.isEditing } : todo
      );

      dispatch(setTodoEdit(saveTask));
      setAlreadyEdit(false);
    } else if (event.key === 'Enter' && event.target.value.trim() === '') {
      event.preventDefault();
    }
  };

  const handleSaveTaskBtn = (id) => {
    const saveTask = todos.map((todo) =>
      todo.id === id ? { ...todo, task: editingTask, isEditing: !todo.isEditing } : todo
    );

    dispatch(setTodoEdit(saveTask));
    setAlreadyEdit(false);
  };

  const handleFilter = (event) => {
    dispatch(setTodoFilter(event));
  };

  let filteredTodos;

  if (todofilter === 'active') {
    filteredTodos = todos.filter((todo) => todo.status === 'active');
    // countActive = filteredTodos ? filteredTodos.reduce((count) => (count += 1), 0) : 0;
  } else if (todofilter === 'completed') {
    filteredTodos = todos.filter((todo) => todo.status === 'completed');
  } else {
    filteredTodos = todos;
  }

  // let icon;

  // if (todofilter === 'active') {
  //   icon = <RadioButtonUncheckedIcon sx={{ color: 'grey' }} />;
  //   // icon = <RadioButtonUncheckedIcon sx={{ color: 'grey' }} />;
  // } else if (todofilter === 'completed') {
  //   if (theme === 'light') {
  //     icon = <CheckCircleIcon sx={{ color: 'blue' }} />;
  //   } else {
  //     icon = <CheckCircleIcon sx={{ color: 'pink' }} />;
  //   }
  //   // icon = theme === 'light' ? <CheckCircleIcon sx={{ color: 'blue' }} /> : <CheckCircleIcon sx={{ color: 'pink' }} />;
  // } else {
  //   icon;
  // }

  const countActive = todos.filter((todo) => todo.status === 'active').length;

  return (
    <div className={classes.list}>
      <Paper component="form" sx={{ display: 'flex', height: '100%', flexDirection: 'column' }}>
        <div className={classes.overflow}>
          {filteredTodos?.map((todo, i) =>
            todo.isEditing ? (
              <div className={theme === 'light' ? classes.inputEdit : classes.inputEditDark} key={i}>
                <IconButton sx={{ p: '10px' }} aria-label="menu" disabled>
                  {theme === 'light' ? (
                    <RadioButtonUncheckedIcon />
                  ) : (
                    <RadioButtonUncheckedIcon sx={{ color: 'grey' }} />
                  )}
                </IconButton>
                <InputBase
                  // component="form"
                  sx={{ ml: 1, flex: 1, fontFamily: `'Josefin Sans', sans-serif` }}
                  placeholder="Input Task"
                  value={editingTask}
                  // inputProps={{ 'aria-label': 'input task' }}
                  onChange={handleTermTask}
                  onKeyDown={(e) => handleSaveTask(e, todo.id)}
                  // onKeyDown={() => handleSaveTask(todo.id)}
                />
                <IconButton sx={{ p: '10px' }} aria-label="menu" onClick={() => handleSaveTaskBtn(todo.id)}>
                  <CheckIcon sx={{ color: 'grey' }} />
                </IconButton>

                <IconButton sx={{ p: '10px' }} aria-label="menu" onClick={() => closeEdit(todo.id, todo.task)}>
                  <ChevronLeftIcon sx={{ color: 'grey' }} />
                </IconButton>
                {/* <InputBase
                  sx={{ ml: 1, flex: 1, fontFamily: `'Josefin Sans', sans-serif` }}
                  placeholder="Input Task"
                  // inputProps={{ 'aria-label': 'input task' }}
                  onChange={handleRemoveTodo}
                  value={todo.task}
                />
                <IconButton sx={{ p: '10px' }} aria-label="menu" onClick={() => handleEditTodo(todo.id)}>
                  <EditIcon sx={{ color: 'grey' }} />
                </IconButton> */}
              </div>
            ) : (
              <div className={theme === 'light' ? classes.arrList : classes.arrListDark} key={i}>
                <IconButton sx={{ p: '10px' }} aria-label="menu" onClick={() => handleActive(todo.id)}>
                  {todo.status === 'active' ? (
                    <RadioButtonUncheckedIcon sx={{ color: 'grey' }} />
                  ) : theme === 'light' ? (
                    <CheckCircleIcon sx={{ color: 'blue' }} />
                  ) : (
                    <CheckCircleIcon sx={{ color: 'pink' }} />
                  )}
                </IconButton>
                <div className={todo.status === 'active' ? classes.fontTitle : classes.fontTitleCompleted}>
                  {todo.task}
                </div>
                <IconButton sx={{ p: '10px' }} aria-label="menu" onClick={() => editTodo(todo.id, todo.task)}>
                  <EditIcon sx={{ color: 'grey' }} />
                </IconButton>
                <IconButton sx={{ p: '10px' }} aria-label="menu" onClick={() => handleRemoveTodo(todo.id)}>
                  <CloseIcon sx={{ color: 'grey' }} />
                </IconButton>
              </div>
            )
          )}
        </div>

        <div className={classes.boxBottom}>
          <div className={classes.left}>{countActive} item left</div>
          <div className={theme === 'light' ? classes.center : classes.centerDark}>
            <Button
              className={todofilter === 'all' ? classes.text : ''}
              onClick={() => handleFilter('all')}
              size="small"
            >
              All
            </Button>
            <Button
              className={todofilter === 'active' ? classes.text : ''}
              onClick={() => handleFilter('active')}
              size="small"
            >
              Active
            </Button>
            <Button
              className={todofilter === 'completed' ? classes.text : ''}
              onClick={() => handleFilter('completed')}
              size="small"
            >
              Completed
            </Button>
          </div>
          <div className={theme === 'light' ? classes.right : classes.rightDark}>
            <Button onClick={() => handleClearCompleted('completed')} size="small">
              Clear Completed
            </Button>
          </div>
        </div>
      </Paper>
      <Paper component="form" sx={{ display: 'flex', flexDirection: 'column', marginTop: '1rem' }}>
        <div className={theme === 'light' ? classes.boxMobile : classes.boxMobileDark}>
          <Button className={todofilter === 'all' ? classes.text : ''} onClick={() => handleFilter('all')} size="small">
            All
          </Button>
          <Button
            className={todofilter === 'active' ? classes.text : ''}
            onClick={() => handleFilter('active')}
            size="small"
          >
            Active
          </Button>
          <Button
            className={todofilter === 'completed' ? classes.text : ''}
            onClick={() => handleFilter('completed')}
            size="small"
          >
            Completed
          </Button>
        </div>
      </Paper>
    </div>
  );
};

List.propTypes = {
  theme: PropTypes.string,
  todos: PropTypes.array,
  todofilter: PropTypes.string,

  // todoisediting: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
  todos: selectTodo,
  todoactive: selectTodoActive,
  todofilter: selectTodoFilter,
});

export default connect(mapStateToProps)(List);
