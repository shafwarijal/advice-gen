import PropTypes from 'prop-types';
import NightsStayIcon from '@mui/icons-material/NightsStay';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import IconButton from '@mui/material/IconButton';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import { setTheme } from '@containers/Settheme/actions';
import { selectTheme } from '@containers/Settheme/selector';
import { useDispatch, connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { setTodo } from '@containers/Settodo/actions';
import uuid from 'react-uuid';
// import { selectTodo } from '@containers/Settodo/selector';

import { selectTodo } from '@containers/Settodo/selector';
import classes from './style.module.scss';

const Top = ({ theme }) => {
  const dispatch = useDispatch();

  const handleInputTodo = (event) => {
    if (event.key === 'Enter' && event.target.value.trim() !== '') {
      event.preventDefault();

      const newTodo = {
        id: uuid(),
        task: event.target.value,
        status: 'active',
        isEditing: false,
      };

      dispatch(setTodo(newTodo));
      event.target.value = '';
    }
  };

  const handleTheme = () => {
    if (theme === 'light') {
      dispatch(setTheme('dark'));
    } else {
      dispatch(setTheme('light'));
    }
  };
  return (
    <div className={classes.top}>
      <div className={classes.title}>
        <div className={classes.logo}>TODO</div>
        <div className={classes.darkmode}>
          <IconButton className={classes.btn} variant="contained" onClick={handleTheme}>
            {theme === 'light' ? (
              <NightsStayIcon sx={{ fontSize: 36, color: 'white' }} />
            ) : (
              <WbSunnyIcon sx={{ fontSize: 36, color: 'white' }} />
            )}
          </IconButton>
        </div>
      </div>
      <div className={classes.boxInput}>
        <Paper component="form" sx={{ p: '0.6rem 1rem', display: 'flex', alignItems: 'center' }}>
          <IconButton sx={{ p: '10px' }} aria-label="menu" disabled>
            {theme === 'light' ? <RadioButtonUncheckedIcon /> : <RadioButtonUncheckedIcon sx={{ color: 'grey' }} />}
          </IconButton>
          <InputBase
            sx={{ ml: 1, flex: 1, fontFamily: `'Josefin Sans', sans-serif` }}
            placeholder="Input Task"
            inputProps={{ 'aria-label': 'input task' }}
            onKeyDown={handleInputTodo}
          />
        </Paper>
      </div>
    </div>
  );
};

Top.propTypes = {
  theme: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
  todos: selectTodo,
});

export default connect(mapStateToProps)(Top);
