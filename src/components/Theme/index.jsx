// import Top from '@components/Top';
// import List from '@components/List';
// import Bottom from '@components/Bottom';
import NightsStayIcon from '@mui/icons-material/NightsStay';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import IconButton from '@mui/material/IconButton';

import PropTypes from 'prop-types';
import { selectTheme } from '@containers/Settheme/selector';
import { createStructuredSelector } from 'reselect';
import { connect, useDispatch } from 'react-redux';
import { setTheme } from '@containers/Settheme/actions';

import classes from './style.module.scss';

const Theme = ({ theme }) => {
  const dispatch = useDispatch();

  const handleTheme = () => {
    if (theme === 'light') {
      dispatch(setTheme('dark'));
    } else {
      dispatch(setTheme('light'));
    }
  };
  return (
    <div className={classes.theme}>
      <IconButton className={classes.btn} variant="contained" onClick={handleTheme}>
        {theme === 'light' ? (
          <WbSunnyIcon sx={{ fontSize: 36, color: 'var(--color-text)' }} />
        ) : (
          <NightsStayIcon sx={{ fontSize: 36, color: 'var(--color-text)' }} />
        )}
      </IconButton>
    </div>
  );
};

Theme.propTypes = {
  theme: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
});

export default connect(mapStateToProps)(Theme);
