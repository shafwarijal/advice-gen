import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect, useDispatch } from 'react-redux';
import { getAdviceApi } from '@containers/Quote/actions';
import { useEffect, useState } from 'react';
import { selectAdvice, selectAdviceLoading } from '@containers/Quote/selectors';
import CasinoIcon from '@mui/icons-material/Casino';
import { TypeAnimation } from 'react-type-animation';
import classes from './style.module.scss';

const Card = ({ advice, adviceloading }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);

  const handleNewAdvice = () => {
    !loading && dispatch(getAdviceApi());
    setLoading(true);
    // dispatch(setAdviceLoading(true));

    setTimeout(() => {
      // dispatch(setAdviceLoading(false));
      setLoading(false);
    }, 3000);
  };

  useEffect(() => {
    dispatch(getAdviceApi());
  }, [dispatch]);

  // console.log(adviceloading);
  // console.log(advice, 'asdasd');

  return (
    <div className={classes.card}>
      {adviceloading ? (
        <TypeAnimation
          cursor={false}
          sequence={['. . . .']}
          wrapper="span"
          speed={20}
          className={classes.text}
          style={{ marginBottom: '2rem' }}
        />
      ) : (
        <>
          <div className={classes.adviceid}>Advice #{advice.id}</div>
          <div className={classes.text}>
            <TypeAnimation sequence={[`“${advice.advice}”`]} wrapper="span" speed={40} />
          </div>
          <div className={classes.line}>
            <div className={classes.boxLineLeft}>
              <div className={classes.lineLeft} />
            </div>
            <div className={classes.boxIcon}>
              <div className={classes.box1} />
              <div className={classes.box2} />
            </div>
            <div className={classes.boxLineRight}>
              <div className={classes.lineRight} />
            </div>
          </div>
        </>
      )}

      <div className={`${classes.button} ${loading === true ? classes.rotate : ''}`} onClick={handleNewAdvice}>
        <CasinoIcon className={classes.iconbtn} />
      </div>
    </div>
  );
};
Card.propTypes = {
  advice: PropTypes.object,
  adviceloading: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  advice: selectAdvice,
  adviceloading: selectAdviceLoading,
});

export default connect(mapStateToProps)(Card);
