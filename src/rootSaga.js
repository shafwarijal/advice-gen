import { all } from 'redux-saga/effects';

import appSaga from '@containers/App/saga';
import adviceSaga from '@containers/Quote/saga';

export default function* rootSaga() {
  yield all([appSaga(), adviceSaga()]);
}
