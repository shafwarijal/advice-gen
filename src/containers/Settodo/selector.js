import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectTodoState = (state) => state.settodo || initialState;

const selectTodo = createSelector(selectTodoState, (state) => state.todos);
const selectTodoFilter = createSelector(selectTodoState, (state) => state.todofilter);
const selectTodoActive = createSelector(selectTodoState, (state) => state.todoactive);

export { selectTodo, selectTodoFilter, selectTodoActive };
