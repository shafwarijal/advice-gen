import { produce } from 'immer';

import {
  SET_TODO,
  SET_TODO_FILTER,
  SET_TODO_REMOVE,
  SET_TODO_ACTIVE,
  SET_TODO_CLEAR,
  SET_TODO_EDIT,
  SET_TODO_ISEDITING,
} from './constants';

export const initialState = {
  todos: [],
  todofilter: 'All',

  // todoremove: [],
};

export const storedKey = ['todos'];

// eslint-disable-next-line default-param-last
const todoReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_TODO:
        draft.todos = [...draft.todos, action.todos];
        break;
      case SET_TODO_FILTER:
        draft.todofilter = action.todofilter;
        break;
      case SET_TODO_REMOVE:
        draft.todos = action.todoremove;
        break;
      case SET_TODO_ACTIVE:
        draft.todos = action.todoactive;
        break;
      case SET_TODO_CLEAR:
        draft.todos = action.todoclear;
        break;
      case SET_TODO_ISEDITING:
        draft.todos = action.todoisediting;
        break;
      case SET_TODO_EDIT:
        draft.todos = action.todoedit;
        break;
      default:
        break;
    }
  });

export default todoReducer;
