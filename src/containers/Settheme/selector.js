import { createSelector } from 'reselect';
import { initialState } from '@containers/Settheme/reducer';

const selectThemeState = (state) => state.settheme || initialState;

const selectTheme = createSelector(selectThemeState, (state) => state.theme);

export { selectTheme };
