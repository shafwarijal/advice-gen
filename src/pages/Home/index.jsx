// import Top from '@components/Top';
// import List from '@components/List';
// import Bottom from '@components/Bottom';
import Card from '@components/Card';
import Theme from '@components/Theme';

import PropTypes from 'prop-types';
import { selectTheme } from '@containers/Settheme/selector';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import classes from './style.module.scss';

const Home = ({ theme }) => {
  const isDark = theme === 'dark';

  return (
    <div className={isDark ? classes.darkTheme : classes.lightTheme}>
      {/* <div className={isDark ? classes.containerImageDark : classes.containerImage} />
        <Top />
        <List />
        <Bottom /> */}
      <Theme />
      <Card />
    </div>
  );
};

Home.propTypes = {
  theme: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
});

export default connect(mapStateToProps)(Home);
